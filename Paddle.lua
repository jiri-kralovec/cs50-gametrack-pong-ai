--[[
    This is CS50 2019.
    Games Track
    Pong

    -- Paddle Class --

    Author: Colton Ogden
    cogden@cs50.harvard.edu

    Represents a paddle that can move up and down. Used in the main
    program to deflect the ball back toward the opponent.
]]

Paddle = Class{}

--[[
    The `init` function on our class is called just once, when the object
    is first created. Used to set up all variables in the class and get it
    ready for use.

    Our Paddle should take an X and a Y, for positioning, as well as a width
    and height for its dimensions.

    Note that `self` is a reference to *this* object, whichever object is
    instantiated at the time this function is called. Different objects can
    have their own x, y, width, and height values, thus serving as containers
    for data. In this sense, they're very similar to structs in C.
]]
function Paddle:init(x, y, width, height, type)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.dy = 0
    -- Player type. 0 for AI
    self.type = type
    self.playerId = self.x < VIRTUAL_WIDTH / 2 and 1 or 2
    self.aiDifficulty = 1
end

function Paddle:update(dt)
    -- player movement
    if self.type == 1 or self.type == 2 then
        if love.keyboard.isDown(self.type == 1 and 'w' or 'up') then
            self.dy = -PADDLE_SPEED
        elseif love.keyboard.isDown(self.type == 1 and 's' or 'down') then
            self.dy = PADDLE_SPEED
        else
            self.dy = 0
        end
    else
        -- limit difficulty
        if self.aiDifficulty < 1 then
            self.aiDifficulty = 1
        elseif self.aiDifficulty > 3 then
            self.aiDifficulty = 3
        end
        local difficulties = { 100, 175, 250 }
        local difficulty = difficulties[self.aiDifficulty]
        -- Check if ball is going against player
        local isBallComingTowardsPlayer = false
        if self.playerId == 1 then
            isBallComingTowardsPlayer = ball.dx < 0
        else
            isBallComingTowardsPlayer = ball.dx > 0
        end
        --[[
            Difficulty check
            This AI uses reaction offset as difficulty. The higher the difficulty, the faster are reactions of player
        ]]
        if isBallComingTowardsPlayer then
            if self.playerId == 1 then
                if ball.x > VIRTUAL_WIDTH - difficulty then
                    self.dy = 0
                    return
                end
            else
                if ball.x < difficulty then
                    self.dy = 0
                    return
                end
            end
        end
        -- Determine target Y
        local moveTo = 0
        if isBallComingTowardsPlayer then
            moveTo = math.floor(Y_PREDICTION - self.height / 2)
        else
            moveTo = math.floor(VIRTUAL_HEIGHT / 2 - self.height / 2)
        end
        -- To avoid repetitive operation, floor Y
        local yFloored = math.floor(self.y);
        -- Start moving towards target
        if yFloored > moveTo then
            -- Calculate difference
            local dif = yFloored - moveTo
            -- If difference is lower than 4, set Y to static value and speed to 0
            if dif < 4 then
                self.y = self.y - dif
                self.dy = 0
            else
                self.dy = -PADDLE_SPEED
            end
        elseif yFloored < moveTo then
            -- Calculate difference
            local dif = moveTo - yFloored
            -- If difference is lower than 4, set Y to static value and speed to 0
            if dif < 4 then
                self.y = self.y - dif
                self.dy = 0
            else
                self.dy = PADDLE_SPEED
            end
        else
            self.dy = 0
        end
    end
    -- math.max here ensures that we're the greater of 0 or the player's
    -- current calculated Y position when pressing up so that we don't
    -- go into the negatives; the movement calculation is simply our
    -- previously-defined paddle speed scaled by dt
    if self.dy < 0 then
        self.y = math.max(0, self.y + self.dy * dt)
    -- similar to before, this time we use math.min to ensure we don't
    -- go any farther than the bottom of the screen minus the paddle's
    -- height (or else it will go partially below, since position is
    -- based on its top left corner)
    else
        self.y = math.min(VIRTUAL_HEIGHT - self.height, self.y + self.dy * dt)
    end
end

--[[
    To be called by our main function in `love.draw`, ideally. Uses
    LÖVE2D's `rectangle` function, which takes in a draw mode as the first
    argument as well as the position and dimensions for the rectangle. To
    change the color, one must call `love.graphics.setColor`. As of the
    newest version of LÖVE2D, you can even draw rounded rectangles!
]]
function Paddle:render()
    love.graphics.rectangle('fill', self.x, self.y, self.width, self.height)
end